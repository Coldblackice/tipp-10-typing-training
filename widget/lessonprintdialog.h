/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

SPDX-License-Identifier: GPL-2.0-only
*/

/****************************************************************
**
** Definition of the LessonPrintDialog class
** File name: lessonprintdialog.h
**
****************************************************************/

#ifndef LESSONPRINTDIALOG_H
#define LESSONPRINTDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QString>
#include <QWidget>

//! The LessonPrintDialog class provides a user input widget.
/*!
        The LessonPrintDialog class shows a user name input for printing.

        @author Tom Thielicke, s712715
        @version 0.0.1
        @date 09.09.2008
*/
class LessonPrintDialog : public QDialog {
    Q_OBJECT

public:
    //! Constructor, creates two table objects and provide it in two tabs.
    LessonPrintDialog(QString* enteredName, QWidget* parent = 0);

public slots:

private slots:

    //! Start button pressed
    void clickOk();

private:
    //! Creates a cancel and a ok button.
    void createButtons();

    //! Creates a textbox.
    void createLineEdit();

    //! Creates the layout of the complete class.
    void createLayout();

    QPushButton* buttonOk;
    QPushButton* buttonCancel;
    QLabel* labelName;
    QLineEdit* lineName;
    QString* userName;
};

#endif // LESSONPRINTDIALOG_H
